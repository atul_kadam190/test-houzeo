<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\User;
use Response;
use Illuminate\Validation\Rule;
use Exception;
use Illuminate\Support\Facades\Redirect;
use App\Country;

class CountryController extends Controller
{
    //
    public function add_country_view(Request $req)
    {
        return view('create-country');
    }

    public function saveCountry(Request $req)
    {
        try {
            $validator = Validator($req->all(),
            array('name' => 'required',
                'state_initial' => 'required',
                'state_fullname' => 'required'));

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }
            
            $country_id = Country::create([
                "name" => $req->name,
                "state_initial" => $req->state_initial,
                "state_fullname" => $req->state_fullname
            ]);
            if($country_id!='')
            {
                return redirect()->route('country.list');
            }
            return Redirect::back();

        } catch (Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    public function listCountries(Request $req)
    {
        try {
            $countries = Country::orderBy('id','desc')->get();
            return view('country-list', ['countries' => $countries]);

        } catch(Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    public function updateCountryView(Request $req)
    {
        try{

            $countries = Country::where('id',$req->id)->first();
            return view('update-country', ['countries' => $countries]);

        } catch(Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    public function updateCountry(Request $req)
    {
        try {
            $country = Country::findOrFail($req->id);
            $validator = Validator($req->all(),
            array('id' => 'required',
                'name' => 'required',
                'state_initial' => 'required',
                'state_fullname' => 'required'));

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }

            $country->name = $req->name;
            $country->state_initial = $req->state_initial;
            $country->state_fullname = $req->state_fullname;
            $country_id = $country->save();
            if($country_id === true)
            {
                return redirect()->route('country.list');
            }
            return Redirect::back();

        } catch (Exception $e){
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    public function delete(Request $req)
    {
        try {
            $validator = Validator($req->all(),
            array('id' => 'required|integer'));

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }
            $country_id = Country::find($req->id)->delete();
            if ($country_id === true) {
                return response()->json([
                    'status' => true,
                    'message' => 'County deleted Successfully'
                ]);
            }
            return response()->json([
                'status' => false,
                'message' => 'Error Occurs While Deleting County'
            ]);
        } catch (Exception $e) {
            // return Redirect::back()->withErrors($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
