<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //this below function is used to display default page
    public function homepage(Request $req)
    {
        return view('home');
    }
}
