<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Response;
use Illuminate\Validation\Rule;
use Exception;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    //
    public function login(Request $req)
    {
        try {
            $validator = Validator($req->all(),
            array('email' => 'required',
                'password' => 'required'));

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }

            $authUser = User::where([['email', $req->email],['password',$req->password]])->first();
        
            if($authUser)
            {
                session(['username' => $authUser->name,
                        'userid' =>$authUser->id,
                        'email'=>$authUser->email]);
                // return Redirect::back();
                // return redirect()->route('country.add');
                return redirect()->route('country.list');
            
            } else {

                $errors=['auth'=>'Creadentials Not Match.'];
                return Redirect::back()->withErrors($errors);
                
            }
        } catch (Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }
}
