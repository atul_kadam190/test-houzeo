<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\User;
use Response;
use Illuminate\Validation\Rule;
use Exception;
use Illuminate\Support\Facades\Redirect;
use App\Mls;
use App\Country;


class MlsController extends Controller
{
    //
    public function add_mls_view(Request $req)
    {
        try {
            $countries=Country::orderBy('id','desc')->get();
            return view('create-mls', ['countries' => $countries]);
        } catch(Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }
    public function saveMls(Request $req)
    {
        try {
            $validator = Validator($req->all(),
            array('name' => 'required',
                'state_initial' => 'required',
                'country_id' => 'required|integer'));

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }

            $mls_id = Mls::create([
                "name" => $req->name,
                "state_initial" => $req->state_initial,
                "country_id" => $req->country_id
            ]);
            if($mls_id!='')
            {
                return redirect()->route('mlscounty.list',$req->country_id);
            }
            return Redirect::back();

        } catch (Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    public function listMls(Request $req) {
        try {
            $mls_list=Mls::with(['country'])->where('country_id',$req->id)->orderBy('id','desc')->get();
            return view('mls-list', ['mls_list' => $mls_list]);
        } catch (Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }


    public function updateMlsView(Request $req)
    {
        try{
            $countries=Country::orderBy('id','desc')->get();
            $mls = Mls::where('id',$req->id)->first();
            return view('update-mls', ['mls' => $mls,'countries'=>$countries]);

        } catch(Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    public function updateMls(Request $req) {
        try {
            $mls = Mls::findOrFail($req->id);
            $validator = Validator($req->all(),
            array('id' => 'required',
                'name' => 'required',
                'state_initial' => 'required',
                'country_id' => 'required|integer'));

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }

            $mls->name = $req->name;
            $mls->state_initial = $req->state_initial;
            $mls->country_id = $req->country_id;
            $mls_id = $mls->save();
            if($mls_id === true)
            {
                return redirect()->route('mlscounty.list',$req->country_id);
            }
            return Redirect::back();
        } catch (Exception $e) {
            return Redirect::back()->withErrors($e->getMessage());
        }
    }

    public function delete(Request $req)
    {
        try {
            
            $validator = Validator($req->all(),
            array('id' => 'required|integer'));

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            }
            $mls_id = Mls::find($req->id)->delete();
            if ($mls_id === true) {
                return response()->json([
                    'status' => true,
                    'message' => 'MLS deleted Successfully'
                ]);
            }
            return response()->json([
                'status' => false,
                'message' => 'Error Occurs While Deleting MLS'
            ]);
        } catch (Exception $e) {
            // return Redirect::back()->withErrors($e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
