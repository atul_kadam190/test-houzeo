<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mls extends Model
{
    //
    protected $guarded = ['id'];
    protected $table = 'mls';

    public function country()
    {
    	return $this->belongsTo('App\Country', 'country_id');
    }
}
