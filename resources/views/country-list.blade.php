<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<style>
.btn {
  border: none;
  color: white;
  padding: 14px 28px;
  font-size: 16px;
  cursor: pointer;
  float:right;
}

.success {background-color: #4CAF50;} /* Green */
.success:hover {background-color: #46a049;}

.info {background-color: #2196F3;} /* Blue */
.info:hover {background: #0b7dda;}

.warning {background-color: #ff9800;} /* Orange */
.warning:hover {background: #e68a00;}

.danger {background-color: #f44336;} /* Red */ 
.danger:hover {background: #da190b;}

.default {background-color: #e7e7e7; color: black;} /* Gray */ 
.default:hover {background: #ddd;}
</style>
</head>
<body>

<h2>Counties List</h2>
<a href="{{route('country.add')}}"><button  class="btn success">Add Counties</button></a>
<!-- <a href="{{route('mls.list')}}"><button  class="btn success">Mls List</button></a> -->
<table>
  <tr>
    <th>Counties Name</th>
    <th>State Initial</th>
    <th>State Full Name</th>
    <th>Action</th>
  </tr>
  @foreach($countries as $key)
  <tr>
    <td>{{$key->name}}</td>
    <td>{{$key->state_initial}}</td>
    <td>{{$key->state_fullname}}</td>
    <td><a href="{{URL::to('/edit-country/'.$key->id)}}"><button  class="btn success">Edit</button></a>
    <button id="delete" value="{{$key->id}}" class="btn success">Delete</button>
    <a href="{{URL::to('/list-mls-county/'.$key->id)}}"><button  class="btn success">Mls List</button></a>
    </td>
  </tr>
  @endforeach
</table>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#delete").click(function(){
    if (confirm("Are you sure?")) {
        $.ajax({url: "delete-country",
        type: 'POST',
        data:{
            "_token": "{{ csrf_token() }}",
            id:$(this).val()
        },
        success: function(result){
        if(result.status==true)
        {
            alert(result.message);
            location.reload();
        }
        else{
            alert(result.message);
            location.reload();
        }
        }});
    }
    return false;
    
  });
});
</script>
</html>
