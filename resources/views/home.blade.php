<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.btn {
  border: none;
  color: white;
  padding: 14px 28px;
  font-size: 16px;
  cursor: pointer;
}

.success {background-color: #4CAF50;} /* Green */
.success:hover {background-color: #46a049;}

.info {background-color: #2196F3;} /* Blue */
.info:hover {background: #0b7dda;}

.warning {background-color: #ff9800;} /* Orange */
.warning:hover {background: #e68a00;}

.danger {background-color: #f44336;} /* Red */ 
.danger:hover {background: #da190b;}

.default {background-color: #e7e7e7; color: black;} /* Gray */ 
.default:hover {background: #ddd;}
</style>
</head>
<body>

<h2>Login Form</h2>

<form id="login_form" method="post" action="{{route('user.login')}}">
@csrf
  <label for="email">Email:</label><br>
  <input type="email" id="email" name="email" value=""></br>
  @if($errors->has('email')) <p class="login_error_msg">{{ $errors->first('email') }}</p> @endif
  </br>
  <label for="password">Password:</label><br>
  <input type="password" id="password" name="password" value="">
  </br>
  @if($errors->has('password')) <p class="login_error_msg">{{ $errors->first('password') }}</p> @endif

  </br>
  </br>
<button class="btn success">Login</button>
</form>


</body>
</html>
