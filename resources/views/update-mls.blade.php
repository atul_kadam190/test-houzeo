<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.btn {
  border: none;
  color: white;
  padding: 14px 28px;
  font-size: 16px;
  cursor: pointer;
}

.success {background-color: #4CAF50;} /* Green */
.success:hover {background-color: #46a049;}

.info {background-color: #2196F3;} /* Blue */
.info:hover {background: #0b7dda;}

.warning {background-color: #ff9800;} /* Orange */
.warning:hover {background: #e68a00;}

.danger {background-color: #f44336;} /* Red */ 
.danger:hover {background: #da190b;}

.default {background-color: #e7e7e7; color: black;} /* Gray */ 
.default:hover {background: #ddd;}
</style>
</head>
<body>

<h2>Update Mls</h2>

<form id="country_form" method="post" action="{{route('mls.update')}}">
@csrf
    <input type="hidden" id="id" name="id" value="{{$mls->id}}">
  <label for="name">Mls Name:</label><br>
  <input type="text" id="name" name="name" value="{{$mls->name}}"></br>
  @if($errors->has('name')) <p class="login_error_msg">{{ $errors->first('name') }}</p> @endif
  </br>

  <label for="state_initial">State Initial:</label><br>
  <input type="text" id="state_initial" name="state_initial" value="{{$mls->state_initial}}"></br>
  @if($errors->has('state_initial')) <p class="login_error_msg">{{ $errors->first('state_initial') }}</p> @endif
  </br>

  <label for="country_id">Choose a County:</label>
  </br>
    <select id="country_id" name="country_id">
    <option value="">Select County</option>
    @foreach ($countries as $cat =>$value)
        <option value="{{$value->id}}" {{ ( $value->id == $mls->country_id) ? 'selected' : '' }}>{{$value->name}}</option>
    @endforeach
    
    </select>
  @if($errors->has('country_id')) <p class="login_error_msg">{{ $errors->first('country_id') }}</p> @endif
  </br>

  
  </br>
  </br>
<button class="btn success">Save</button>
</form>


</body>
</html>
