<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@homepage');
Route::post('login','LoginController@login')->name('user.login');
Route::get('add-country','CountryController@add_country_view')->name('country.add');
Route::post('save-country','CountryController@saveCountry')->name('country.save');
Route::get('list-country','CountryController@listCountries')->name('country.list');

Route::get('edit-country/{id}','CountryController@updateCountryView')->name('country.edit');

Route::post('update-country','CountryController@updateCountry')->name('country.update');
Route::post('delete-country','CountryController@delete')->name('country.delete');

Route::post('save-mls','MlsController@saveMls')->name('mls.save');
Route::get('list-mls','MlsController@listMls')->name('mls.list');
Route::get('list-mls-county/{id}','MlsController@listMls')->name('mlscounty.list');

Route::post('update-mls','MlsController@updateMls')->name('mls.update');
Route::post('delete-mls','MlsController@delete')->name('mls.delete');
Route::get('add-mls','MlsController@add_mls_view')->name('mls.add');

Route::get('edit-mls/{id}','MlsController@updateMlsView')->name('mls.edit');